#Analiza Facebook mreže, Maja Grubišić

import networkx as nx
import matplotlib.pyplot as plt
from operator import itemgetter
import community, numpy, matplotlib, subprocess
from scipy.cluster import hierarchy
from scipy.spatial import distance

#Izbornik

while True:
	odg = input('Izaberite:\n0 za izlaz\n1 za analizu Facebook mreže\n2 za analizu mreže interakcija u grupi \'Airsoft Makarska\'\n')

	if odg == '1':
		#Učitavamo graf

		graph = nx.read_edgelist("edges.txt")

		#Otvaramo datoteku za ispis
		file = open ("PersonalFacebookOUT.txt", 'a');

		N = graph.number_of_nodes()
		K = graph.number_of_edges()
		print ("N = " + str(N))
		print ("K = " + str(K))

		avg_degree = 2*K/N
		print("Prosječni stupanj = " + str(avg_degree))

		dijametar = nx.diameter(graph)
		print("Dijametar = " + str(dijametar))

		avg_shortest_path = nx.average_shortest_path_length(graph)
		print("Prosječna duljina najkraćeg puta = " + str(avg_shortest_path))

		#Mjere centralnosti
		dc = nx.degree_centrality(graph)
		degree_centrality = (sorted(dc.items(), key = itemgetter(1), reverse = True))[:10]

		cc = nx.closeness_centrality(graph)
		closeness_centrality = (sorted(cc.items(), key = itemgetter(1), reverse = True))[:10]

		bc = nx.betweenness_centrality(graph)
		betweenness_centrality = (sorted(bc.items(), key = itemgetter(1), reverse = True))[:10]

		ec = nx.eigenvector_centrality(graph)
		eigenvector_centrality = (sorted(ec.items(), key = itemgetter(1), reverse = True))[:10]



		file.write("\n___________________________________________________________________________________\n\n")
		file.write("10 najcentralnijih čvorova osobne mreže\n")
		file.write("___________________________________________________________________________________\n\n")

		file.write("centralnost stupnja čvora = " + str(degree_centrality) + "\n\n")
		file.write("centralnost blizine = " + str(closeness_centrality) + "\n\n")
		file.write("centralnost međupoloženosti = " + str(betweenness_centrality) + "\n\n")
		file.write("centralnost svojstvenog vektora = " + str(eigenvector_centrality) + "\n\n\n")

		#Koeficijenti grupiranja
		clust_coef = str(nx.clustering(graph))
		clust_coef_avg = str(nx.average_clustering(graph))
		file.write("koeficijent grupiranja = " + clust_coef + "\n\n")
		print ("Prosječni koef.grupiranja: " + clust_coef_avg)

		conn_comp = str(nx.number_connected_components(graph))
		print ("Broj povezanih komponenti iznosi " + conn_comp)

		#Histogram koji prikazuje distribuciju stupnjeva
		degree_distr = nx.degree(graph)

		plt.figure()
		plt.hist(list(degree_distr.values()), histtype='bar')
		plt.title("Degree histogram")
		plt.ylabel("number of nodes")
		plt.xlabel("degree")
		plt.savefig("images/personal_histogram.png")

		density=nx.density(graph)
		print ("Gustoća mreže: " + str(density))

		assortativity = nx.degree_assortativity_coefficient(graph)
		print("Asortativnost: " + str(assortativity))
		pearson_assortativity = nx.degree_pearson_correlation_coefficient(graph)
		print("Asortativnost korištenjem Pearson funkcije: " + str(pearson_assortativity))

		part = community.best_partition(graph)
		modularity = community.modularity(part, graph)
		print ("Modularnost: " + str(modularity))

		#Generiranje dendograma
		dendogram = community.generate_dendogram(graph)
		for level in range(len(dendogram) - 1):
		    file.write("\n\nparticija na razini " + str(level) + " je " + str(community.partition_at_level(dendogram, level)))

		#Generiranje ER grafa i računanje osnovnih mjera istoga
		graph_rand=nx.erdos_renyi_graph(311,0.0572)
		N_rand = graph_rand.number_of_nodes()
		K_rand = graph_rand.number_of_edges()
		print ("ER N = " + str(N_rand))
		print ("ER K = " + str(K_rand))

		L_rand = nx.average_shortest_path_length(graph_rand)
		print ("ER prosječna duljina puta: " + str(L_rand))
		rand_clust_coeff = nx.average_clustering(graph_rand)
		print ("ER koef.grupiranja: " + str(rand_clust_coeff))
		D_rand = nx.diameter(graph_rand)
		print ("ER dijametar: " + str(D_rand))

		#Analiza zajednica mreže
		cl = list(nx.find_cliques(graph))
		no_cl = nx.graph_clique_number(graph)
		max_clique = nx.make_max_clique_graph(graph)
		nx.write_graphml(max_clique, "graphml/personal_max_clique_graph.graphml")
		no_max_cl = nx.graph_number_of_cliques(graph)

		file.write ("Klike: " + str(cl))
		print ("Broj klika je " + str(no_cl))
		print ("Broj maximalnih klika: " + str(no_max_cl) + "\n\n")

		#Louvainov algoritam
		file.write("\n\n___________________________________________________________________________________\n\n")
		file.write("Louvainov algoritam \n")
		file.write("___________________________________________________________________________________\n")

		#drawing
		plt.figure()
		size = float(len(set(part.values())))
		pos = nx.spring_layout(graph)
		count = 0.
		for i in set(part.values()) :
		    file.write ("\n\nZajednica " + str(i) + "\n\n")
		    members = list_nodes = [nodes for nodes in part.keys() if part[nodes] == i]
		    file.write (str(members))
		    
		    count = count + 1.
		    nx.draw_networkx_nodes(graph, pos, list_nodes, node_size = 20,
		                                node_color = str(count / size))


		nx.draw_networkx_edges(graph, pos, alpha=0.5)
		plt.savefig('images/personal_louvain.png')
                # Hijerarhijsko grupiranje, bottom-up
##		path_length = nx.all_pairs_shortest_path_length(graph)
##		n = len(graph.nodes())
##		distances = numpy.zeros((n,n))
##		for u,p in path_length.items():
##		    for v, d in p.items():
##		        distances[int(u)-1][int(v)-1] = d
##		sd = distance.squareform(distances)
##
##		hier = hierarchy.average(sd)
##		file.write("\n\nHijararhijsko grupiranje, bottom-up \n\n" + str(hier) + "\n\n")
##		hierarchy.dendrogram(hier)
##		matplotlib.pylab.savefig("dendrogram_hijer.png")

		#Ego graf
		(largest_node, deg) = sorted(degree_distr.items(), key = itemgetter(1))[-1]
		hub_ego = nx.ego_graph(graph, largest_node)
		nx.write_graphml(hub_ego, "graphml/personal_ego_graf.graphml")
		# Draw graph
		pos=nx.spring_layout(hub_ego)
		nx.draw(hub_ego,pos,node_color='b',node_size=50,with_labels=False)
		# Draw ego as large and red
		nx.draw_networkx_nodes(hub_ego,pos,nodelist=[largest_node],node_size=300,node_color='r')
		plt.savefig('images/personal_ego_graph.png')


		#Različite vizualizacije grafa

		plt.figure()
		nx.draw(graph)
		plt.savefig("images/personal_graph.png")
		plt.figure()
		nx.draw_networkx(graph, with_labels=False)
		plt.savefig("images/personal_graph1.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.spring_layout(graph), with_labels=False)
		plt.savefig("images/personal_graph_springlayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.circular_layout(graph), with_labels=False)
		plt.savefig("images/personal_graph_circularlayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.shell_layout(graph), with_labels=False)
		plt.savefig("images/personal_graph_shelllayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.spectral_layout(graph), with_labels=False)
		plt.savefig("images/personal_graph_spectrallayout.png")


		file.close()

	elif odg == '2':
		#Učitavamo graf

		graph = nx.read_weighted_edgelist("edgesGroup.txt", create_using=nx.DiGraph())

		#Otvaramo datoteku za ispis
		file = open ("AirsoftGroupOUT.txt", 'a');

		N = graph.number_of_nodes()
		K = graph.number_of_edges()
		print ("N = " + str(N))
		print ("K = " + str(K))

		avg_degree = 2*K/N
		print("Prosječni stupanj = " + str(avg_degree))

		graph = graph.to_undirected()
		largest_component = nx.connected_component_subgraphs(graph)[0]
		nodes_lcomp = largest_component.number_of_nodes()
		edges_lcomp = largest_component.number_of_edges()
		print ('Broj čvorova i veza najveće komponente grafa: ' + str(nodes_lcomp) + " - " + str(edges_lcomp))
		conn_comp = str(nx.number_connected_components(graph))
		print ("Broj povezanih komponenti iznosi " + conn_comp)

		dijametar = nx.diameter(largest_component)
		print("Dijametar = " + str(dijametar))

		avg_shortest_path = nx.average_shortest_path_length(largest_component)
		print("Prosječna duljina najkraćeg puta = " + str(avg_shortest_path))

		#Mjere centralnosti
		dc = nx.degree_centrality(graph)
		degree_centrality = (sorted(dc.items(), key = itemgetter(1), reverse = True))[:10]

		cc = nx.closeness_centrality(graph)
		closeness_centrality = (sorted(cc.items(), key = itemgetter(1), reverse = True))[:10]

		bc = nx.betweenness_centrality(graph)
		betweenness_centrality = (sorted(bc.items(), key = itemgetter(1), reverse = True))[:10]

		ec = nx.eigenvector_centrality(graph)
		eigenvector_centrality = (sorted(ec.items(), key = itemgetter(1), reverse = True))[:10]



		file.write("\n___________________________________________________________________________________\n\n")
		file.write("10 najcentralnijih čvorova grupe 'Airsoft Makarska'\n")
		file.write("___________________________________________________________________________________\n\n")

		file.write("centralnost stupnja čvora = " + str(degree_centrality) + "\n\n")
		file.write("centralnost blizine = " + str(closeness_centrality) + "\n\n")
		file.write("centralnost međupoloženosti = " + str(betweenness_centrality) + "\n\n")
		file.write("centralnost svojstvenog vektora = " + str(eigenvector_centrality) + "\n\n\n")

		#Koeficijenti grupiranja
		clust_coef = str(nx.clustering(graph))
		clust_coef_avg = str(nx.average_clustering(graph))
		file.write("koeficijent grupiranja = " + clust_coef + "\n\n")
		print ("Prosječni koef.grupiranja: " + clust_coef_avg)

		#Histogram koji prikazuje distribuciju stupnjeva
		degree_distr = nx.degree(graph)

		plt.figure()
		plt.hist(list(degree_distr.values()), histtype='bar')
		plt.title("Degree histogram")
		plt.ylabel("number of nodes")
		plt.xlabel("degree")
		plt.savefig("images/group_histogram.png")

		density=nx.density(graph)
		print ("Gustoća mreže: " + str(density))

		assortativity = nx.degree_assortativity_coefficient(graph)
		print("Asortativnost: " + str(assortativity))
		pearson_assortativity = nx.degree_pearson_correlation_coefficient(graph)
		print("Asortativnost korištenjem Pearson funkcije: " + str(pearson_assortativity))

		part = community.best_partition(graph)
		modularity = community.modularity(part, graph)
		print ("Modularnost: " + str(modularity))

		#Generiranje dendograma
		dendogram = community.generate_dendogram(graph)
		for level in range(len(dendogram) - 1):
		    file.write("\n\nparticija na razini " + str(level) + " je " + str(community.partition_at_level(dendogram, level)))

		#Generiranje ER grafa i računanje osnovnih mjera istoga
		graph_rand=nx.erdos_renyi_graph(58,0.2)
		N_rand = graph_rand.number_of_nodes()
		K_rand = graph_rand.number_of_edges()
		print ("ER N = " + str(N_rand))
		print ("ER K = " + str(K_rand))

		L_rand = nx.average_shortest_path_length(graph_rand)
		print ("ER prosječna duljina puta: " + str(L_rand))
		rand_clust_coeff = nx.average_clustering(graph_rand)
		print ("ER koef.grupiranja: " + str(rand_clust_coeff))
		D_rand = nx.diameter(graph_rand)
		print ("ER dijametar: " + str(D_rand))

		#Analiza zajednica mreže
		cl = list(nx.find_cliques(graph))
		no_cl = nx.graph_clique_number(graph)
		max_clique = nx.make_max_clique_graph(graph)
		nx.write_graphml(max_clique, "graphml/group_max_clique_graph.graphml")
		no_max_cl = nx.graph_number_of_cliques(graph)

		file.write ("Klike: " + str(cl))
		print ("Broj klika je " + str(no_cl))
		print ("Broj maximalnih klika: " + str(no_max_cl) + "\n\n")

		#Louvainov algoritam
		file.write("\n\n___________________________________________________________________________________\n\n")
		file.write("Louvainov algoritam \n")
		file.write("___________________________________________________________________________________\n")

		#drawing
		plt.figure()
		size = float(len(set(part.values())))
		pos = nx.spring_layout(graph)
		count = 0.
		for i in set(part.values()) :
		    file.write ("\n\nZajednica " + str(i) + "\n\n")
		    members = list_nodes = [nodes for nodes in part.keys() if part[nodes] == i]
		    file.write (str(members))
		    
		    count = count + 1.
		    nx.draw_networkx_nodes(graph, pos, list_nodes, node_size = 20,
		                                node_color = str(count / size))


		nx.draw_networkx_edges(graph, pos, alpha=0.5)
		plt.savefig('images/group_louvain.png')


		#Ego graf
		(largest_node, deg) = sorted(degree_distr.items(), key = itemgetter(1))[-1]
		hub_ego = nx.ego_graph(graph, largest_node)
		nx.write_graphml(hub_ego, "graphml/group_ego_graf.graphml")
		# Draw graph
		pos=nx.spring_layout(hub_ego)
		nx.draw(hub_ego,pos,node_color='b',node_size=50,with_labels=False)
		# Draw ego as large and red
		nx.draw_networkx_nodes(hub_ego,pos,nodelist=[largest_node],node_size=300,node_color='r')
		plt.savefig('images/group_ego_graph.png')


		#Različite vizualizacije grafa

		plt.figure()
		nx.draw(graph)
		plt.savefig("images/group_graph.png")
		plt.figure()
		nx.draw_networkx(graph, with_labels=False)
		plt.savefig("images/group_graph1.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.spring_layout(graph), with_labels=False)
		plt.savefig("images/group_graph_springlayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.circular_layout(graph), with_labels=False)
		plt.savefig("images/group_graph_circularlayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.shell_layout(graph), with_labels=False)
		plt.savefig("images/group_graph_shelllayout.png")
		plt.figure()
		nx.draw_networkx(graph, pos=nx.spectral_layout(graph), with_labels=False)
		plt.savefig("images/group_graph_spectrallayout.png")


		file.close()

	else:
		break
